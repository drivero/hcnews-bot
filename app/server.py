import os
import requests
import time

from flask import Flask, request, Response

from bot import process_command_message

app = Flask(__name__)

BOT_TOKEN = 'xoxb-182606775493-ESD08DxLkvrntO5n1iWK0FX3'
BOT_USER_ID = 'U5CHUNTEH'

registered_ts = []

@app.route('/')
def home():
    return 'The server is working'

@app.route('/receive-message', methods=['POST'])
def receive_message():
    """
        Example of format of request
        {
            "token": "esqGEg3RqDUc6p2WIowY6y56",
            "team_id": "T03HUREA8",
            "api_app_id":"A4P6M6P7S",
            "event": {
                "type": "message",
                "user": "U3MRM0PGW",
                "text": "test",
                "ts": "1494560202.102901",
                "channel": "D5BTSLGGH",
                "event_ts": "1494560202.102901"
            },
            "type": "event_callback",
            "authed_users": ["U5CHUNTEH"],
            "event_id": "Ev5D9XR5N3",
            "event_time": 1494560202
        }

        :return: Response
    """
    channel = request.json['event']['channel']
    user = request.json['event']['user']
    text = request.json['event']['text']
    ts = request.json['event']['ts']

    if (user != BOT_USER_ID) and (ts not in registered_ts):
        result = process_command_message(text)
        registered_ts.append(ts)

        if result.get('status') == 'success':
            topic = ' '.join(text.split(' ')[1:])

            requests.post(
                'https://slack.com/api/chat.postMessage',
                data={
                    'token': BOT_TOKEN,
                    'channel': channel,
                    'text': 'These are the '
                            'latest news about "{}"'.format(topic),
                }
            )

            for new_result in result.get('result'):
                requests.post(
                    'https://slack.com/api/chat.postMessage',
                    data={
                        'token': BOT_TOKEN,
                        'channel': channel,
                        'text': new_result.get('webUrl'),
                        'unfurl_links': True,
                        'unfurl_media': True
                    }
                )
                time.sleep(3)
        else:
            requests.post(
                'https://slack.com/api/chat.postMessage',
                data={
                    'token': BOT_TOKEN,
                    'channel': channel,
                    'text': result.get('message')
                }
            )

        return Response('Ok', status=200)

    return None

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 8000))

    try:
        app.run(host='0.0.0.0', port=port)
    except PermissionError:
        app.run(host='127.0.0.1', port=port)
